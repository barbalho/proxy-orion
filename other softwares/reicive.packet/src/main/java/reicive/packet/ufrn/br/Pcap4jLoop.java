package reicive.packet.ufrn.br;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.pcap4j.core.BpfProgram.BpfCompileMode;
import org.pcap4j.core.NotOpenException;
import org.pcap4j.core.PacketListener;
import org.pcap4j.core.PcapHandle;
import org.pcap4j.core.PcapNativeException;
import org.pcap4j.core.PcapNetworkInterface;
import org.pcap4j.core.PcapNetworkInterface.PromiscuousMode;
import org.pcap4j.core.Pcaps;
import org.pcap4j.packet.IpV4Packet;
import org.pcap4j.packet.Packet;
import org.pcap4j.packet.TcpPacket;
import org.pcap4j.packet.namednumber.TcpPort;
import org.pcap4j.util.NifSelector;

public class Pcap4jLoop {

	public static void main(String[] args)
			throws PcapNativeException, IOException, NotOpenException, InterruptedException {
		String filter = "127.0.0.1";
		if (args.length != 0) {
			filter = args[0];
		}

		InetAddress addr = InetAddress.getByName(filter);
		PcapNetworkInterface nif = Pcaps.getDevByAddress(addr);

//		List<PcapNetworkInterface> networks = Pcaps.findAllDevs();
//		for (PcapNetworkInterface n : networks) {
//			System.out.println(n.getName() + " : " + n.getAddresses());
//		}

//		PcapNetworkInterface nif = new NifSelector().selectNetworkInterface();
		if (nif == null) {
			System.exit(1);
		}

		final PcapHandle handle = nif.openLive(65536, PromiscuousMode.PROMISCUOUS, 10);

//		if (filter != null && filter.length() != 0) {
//			handle.setFilter(filter, BpfCompileMode.OPTIMIZE);
//		}

		PacketListener listener = new PacketListener() {
			public void gotPacket(Packet packet) {
				printPacket(packet, handle);
			}
		};

		
		while(flag){
			handle.loop(5, listener);
		}
		socket("10.0.0.4", 1028);
//		System.out.println(ip+", "+port);
//		socket(ip, port);
		
		System.out.println("---Fim---");
	}
	
	static int qtPackets = 0;
	static boolean flag = true;
	static  byte[] data;
	
	static String ip;
	static int port;
	
	private static void printPacket(Packet packet, PcapHandle ph) {
//		System.out.println(++qtPackets);
		
		IpV4Packet ipV4Packet = packet.get(IpV4Packet.class);
		if (ipV4Packet != null) {

			Inet4Address srcAddr = ipV4Packet.getHeader().getSrcAddr();
			Inet4Address dstAddr = ipV4Packet.getHeader().getDstAddr();
			TcpPacket packetTCP = ipV4Packet.getPayload().get(TcpPacket.class);

//			if(! srcAddr.toString().equals("10.0.0.1"))
//				return;
			
			if (packetTCP != null) {

				TcpPort srcPort = packetTCP.getHeader().getSrcPort();
				TcpPort dstPort = packetTCP.getHeader().getDstPort();
//				System.out.println(srcAddr.toString()+", "+ srcPort.valueAsInt());
				//byte[] data = packetTCP.getPayload().getRawData();
				
//				System.out.println(packetTCP.getHeader().getSyn()+ " - "+srcAddr + ":" + srcPort.valueAsInt() + " -> " + dstAddr + ":" + dstPort.valueAsInt());
				
//				if(dstPort.valueAsInt() != 5000)
//					return;
				
//				if(packetTCP.getHeader().getSyn() && flag && dstPort.valueAsInt() == 5000){
				if(packetTCP.getHeader().getAck() && packetTCP.getHeader().getPsh() && flag && dstPort.valueAsInt() == 5200){
					ip = srcAddr.toString();
					port = srcPort.valueAsInt();
					data = packetTCP.getPayload().getRawData();
//					socket(srcAddr.toString(), srcPort.valueAsInt());
					System.out.println(srcAddr.toString()+", "+ srcPort.valueAsInt());
//					socket("10.0.0.3", 1028);
//					System.out.println(srcAddr.toString()+", "+ srcPort.valueAsInt());
					flag = false;
				}

//				System.out.println(packetTCP.getHeader().getSyn()+ " - "+srcAddr + ":" + srcPort.valueAsInt() + " -> " + dstAddr + ":" + dstPort.valueAsInt());
			}
		}

		// TcpPacket packetTCP = packet.get(TcpPacket.class);
		// if(packetTCP != null){
		// TcpPort srcPort = packetTCP.getHeader().getSrcPort();
		// TcpPort dstPort = packetTCP.getHeader().getDstPort();
		// byte[] data = packetTCP.getPayload().getRawData();
		//
		// System.out.println(data.length);
		// }

//		 StringBuilder sb = new StringBuilder();
//		 sb.append("A packet captured at ")
//		 .append(ph.getTimestamp())
//		 .append(":");
//		 System.out.println(sb);
//		 System.out.println(packet);
	}
	
	public static void socket(String ip, int port){
		String sData = "";
		for(byte d : data){
			sData += (char) d;
		}
//		System.out.println(sData);
		sData = sData.substring(sData.indexOf("{"));
		try {
			String USER_AGENT = "Mozilla/5.0";
//			String url = "http://127.0.0.1:1028/accumulate";
			if(ip.charAt(0) != '/')
				ip = '/'+ip;
			String url = "http:/"+ip+":"+port+"/accumulate";
			
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
//			con.setRequestMethod("GET");
			

			//add request header
//			con.setRequestProperty("User-Agent", USER_AGENT);
			
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", "orion/1.6.0-next libcurl/7.19.7");
			con.setRequestProperty("Host", "10.0.0.2:1040");
			con.setRequestProperty("Accept", "application/json");
			con.setRequestProperty("Content-length", "478");
			con.setRequestProperty("Content-type", "application/json; charset=utf-8");
			con.setRequestProperty("Fiware-Correlator", "478er343434erwer345");
			con.setDoOutput(true);
			con.getOutputStream().write(sData.getBytes());
			
			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			//print result
			System.out.println(response.toString());
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		try {
//			
//			
//			
////			//Socket client = new Socket("127.0.0.1",1028);
////			System.out.println(client.getLocalAddress());
////			client.close();
//		} catch (UnknownHostException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
	}

	// public static void main( String[] args ){
	//
	//
	// InetAddress addr;
	// try {
	// addr = InetAddress.getByName("192.168.0.103");
	//// addr = InetAddress.getLocalHost();
	// PcapNetworkInterface nif = Pcaps.getDevByAddress(addr);
	//// List<PcapNetworkInterface> networks = Pcaps.findAllDevs();
	////
	//// for (PcapNetworkInterface n : networks){
	//// System.out.println(n.getName()+" : "+n.getAddresses());
	//// }
	//
	//
	// int snapLen = 65536;
	// PromiscuousMode mode = PromiscuousMode.PROMISCUOUS;
	// int timeout = 10000;
	// PcapHandle handle = nif.openLive(snapLen, mode, timeout);
	//
	// Packet packet = handle.getNextPacketEx();
	// handle.close();
	//
	// IpV4Packet ipV4Packet = packet.get(IpV4Packet.class);
	// Inet4Address srcAddr = ipV4Packet.getHeader().getSrcAddr();
	// TcpPacket tcp = ipV4Packet.getPayload().get(TcpPacket.class);
	// System.out.println(srcAddr);
	//
	// } catch (UnknownHostException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (PcapNativeException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (EOFException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (TimeoutException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (NotOpenException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	//
	// System.out.println( "Hello World!" );
	// }
	
	

}
