<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
    	<title>Aplicação consumidora de dados do Orion Context Broker</title>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<style type="text/css">
	        div {
	            width: 600px;
	            height: 600px;
	        }
	        .loader {
	            display: none;
	            float: left;
	        }
    	</style>
	</head>

	<body>

		<form action="crossDomainOrionCB.php" method="post">
			<table>
				<tr>
					<td><label for="orionaddress">Orion address:</label></td>
					<td><input type="text" name="orionaddress" id="orionaddress"  value="localhost"/></td>
					<td><label onclick="test('orionaddress','localhost')" for="txtorionaddress">e.g., localhost</label></td>
				</tr>

				<tr>
					<td><label for="type">type:</label></td>
					<td><input type="text" name="type" id="type" placeholder="Room" /></td>
					<td><label onclick="test('type','Room')" for="txttype">e.g., Room</label></td>
				</tr>

				<tr>
					<td><label for="isPattern">isPattern:</label></td>
					<td><input type="text" name="isPattern" id="isPattern" placeholder="false"/></td>
					<td><label onclick="test('isPattern','false')" for="txtisPattern">e.g., false</label></td>
				</tr>

				<tr>
					<td><label for="id">id:</label></td>
					<td><input type="text" name="id" id="id" placeholder="Room2"/></td>
					<td><label onclick="test('id','Room2')" for="txtid">e.g., Room2</label></td>
				</tr>

				<tr>
					<td><label for="attributes">attributes:</label></td>
					<td><input type="text" name="attributes" id="attributes" placeholder="temperature"/></td>
					<td><label onclick="test('attributes','temperature')" for="txtattributes">e.g., temperature</label></td>
				</tr>

				<tr>
					<td><label for="reference">reference:</label></td>
					<td><input type="text" name="reference" id="reference" placeholder="http://localhost:1028/accumulate"/></td>
					<td><label onclick="test('reference','http://localhost:1028/accumulate')" for="txtreference">e.g., http://localhost:1028/accumulate</label></td>
				</tr>

				<tr>
					<td><label for="duration">duration:</label></td>
					<td><input type="text" name="duration" id="duration" placeholder="PT5S"/></td>
					<td><label onclick="test('duration','PT5S')" for="txtduration">e.g., PT5S</label></td>
				</tr>

				<tr>
					<td><label for="condValues">condValues:</label></td>
					<td><input type="text" name="condValues" id="condValues" placeholder="temperature" /></td>
					<td><label onclick="test('condValues','temperature')" for="txtcondValues">e.g., temperature</label></td>
				</tr>
			</table>

			<input type="button" value="Query Orion" onclick="subm()"/>

		</form>

	

<!-- 	<?php
		$type = $_POST['type'];
		$isPattern = $_POST['isPattern'];
		$id = $_POST['id'];
		$attributes = $_POST['attributes'];
	?> -->

	<script type="text/javascript" src="js/jquery-1.4.3.min.js"></script>
	<!-- <script type="text/javascript" src="/var/www/html/OCB.js"></script> -->
	<script type="text/javascript">

		function test(idobject, value){
			document.getElementById(idobject).value = value;
		}

		function subm(){

	
      	//$('input').click(function(){ //Quando clicado no elemento input

  		var orionaddress = document.getElementById('orionaddress').value;
		var type = document.getElementById('type').value;
		var isPattern = document.getElementById('isPattern').value;
		var id = document.getElementById('id').value;
		var attributes = document.getElementById('attributes').value;
		var reference = document.getElementById('reference').value;
		var duration = document.getElementById('duration').value;
		var condValues = document.getElementById('condValues').value;
	 	var element = {
			"entities":
			[
				{
					"type":type,
					"isPattern":isPattern,
					"id":id
				}
			],
			"attributes":
			[
				attributes
			],
			"reference": reference,
			"duration": duration,
			"notifyConditions": [
				{
					"type": "ONCHANGE",
					"condValues": [
						condValues
					]
				}
				],"throttling": "PT5S"
		};
	

		$.ajax ({
		  	url: 'crossDomainOrionCB.php',
		  	timeout: '10000',
		  	type: 'POST',
			dataType: 'json',
			data:{
			  	"data":JSON.stringify(element),
			  	"ocbIP":'127.0.0.1',
			  	"ocbPort":1026
   		  	},
		  	success:function(data){
				console.log("Success", data);//return respons from Orion server
			},
			error:function(e){
				console.log("Error", e);//retur ajax error
			}
		});


		alert("subscriber submitted");
	}
    
	</script>

	</body>

</html>
