package floodlight.orion.listsubs;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class App {
	
	public static String host = "10.0.0.1";
	public static final String port = "1026";
	public static String href = ":1026/v2/subscriptions";
	
    public static void main( String[] args ){
    	
    	if(args.length > 0){
    		
    		host = args[0];
    	}
    	
    	href = "http://"+host+href;
    	System.out.println(href);

    	String stringResponse = consultIds();
    	//String stringResponse = readFile();
    	
		try {
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(stringResponse);
			JSONArray jsonObjects = (JSONArray) obj;
			for(Object js : jsonObjects){
				String subscription = (String) ((JSONObject) js).get("id");
				System.out.println(subscription);
				deleteSubscription(subscription);
			}
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public static String consultIds(){
    	Client client = ClientBuilder.newClient();
		Response response = client.target(href).request().method("GET");
		System.out.println(response.getStatus());
		String stringResponse = response.readEntity(String.class);
		return stringResponse;
    }
    
    public static boolean deleteSubscription(String id){
    	Client client = ClientBuilder.newClient();
		Response response = client.target(href+"/"+id).request().method("DELETE");
		System.out.println(response.getStatus());
		return true;
    }
    
    public static String readFile(){
    	 try {
			String contents = new String(Files.readAllBytes(Paths.get("subscriptions.txt")));
			return contents;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
    }
}
