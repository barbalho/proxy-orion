curl 10.0.0.1:1026/v2/entities -s -S --header 'Content-Type: application/json' -d @- <<EOF
{
  "id": "Room2",
  "type": "Room",
  "temperature": {
    "value": 30,
    "type": "Float"
  },
  "pressure": {
    "value": 400,
    "type": "Integer"
  }
}
EOF