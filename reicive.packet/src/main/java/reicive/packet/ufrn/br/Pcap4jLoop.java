package reicive.packet.ufrn.br;

import java.io.IOException;
import java.net.Inet4Address;

import org.pcap4j.core.BpfProgram.BpfCompileMode;
import org.pcap4j.core.NotOpenException;
import org.pcap4j.core.PacketListener;
import org.pcap4j.core.PcapHandle;
import org.pcap4j.core.PcapNativeException;
import org.pcap4j.core.PcapNetworkInterface;
import org.pcap4j.core.PcapNetworkInterface.PromiscuousMode;
import org.pcap4j.packet.IpV4Packet;
import org.pcap4j.packet.Packet;
import org.pcap4j.packet.TcpPacket;
import org.pcap4j.packet.namednumber.TcpPort;
import org.pcap4j.util.NifSelector;

public class Pcap4jLoop {

	public static void main(String[] args)
			throws PcapNativeException, IOException, NotOpenException, InterruptedException {
		String filter = null;
		if (args.length != 0) {
			filter = args[0];
		}

		PcapNetworkInterface nif = new NifSelector().selectNetworkInterface();
		if (nif == null) {
			System.exit(1);
		}

		final PcapHandle handle = nif.openLive(65536, PromiscuousMode.PROMISCUOUS, 10);

		if (filter != null && filter.length() != 0) {
			handle.setFilter(filter, BpfCompileMode.OPTIMIZE);
		}

		PacketListener listener = new PacketListener() {
			public void gotPacket(Packet packet) {
				printPacket(packet, handle);
			}
			
			
		};

		handle.loop(5, listener);
	}

	private static void printPacket(Packet packet, PcapHandle ph) {

		IpV4Packet ipV4Packet = packet.get(IpV4Packet.class);
		if (ipV4Packet != null) {

			Inet4Address srcAddr = ipV4Packet.getHeader().getSrcAddr();
			Inet4Address dstAddr = ipV4Packet.getHeader().getDstAddr();
			TcpPacket packetTCP = ipV4Packet.getPayload().get(TcpPacket.class);

			if (packetTCP != null) {

				TcpPort srcPort = packetTCP.getHeader().getSrcPort();
				TcpPort dstPort = packetTCP.getHeader().getDstPort();
				byte[] data = packetTCP.getPayload().getRawData();

				System.out.println(srcAddr + ":" + srcPort.valueAsInt() + " -> " + dstAddr + ":" + dstPort.valueAsInt()
						+ " = " + data.length);
			}
		}

		// TcpPacket packetTCP = packet.get(TcpPacket.class);
		// if(packetTCP != null){
		// TcpPort srcPort = packetTCP.getHeader().getSrcPort();
		// TcpPort dstPort = packetTCP.getHeader().getDstPort();
		// byte[] data = packetTCP.getPayload().getRawData();
		//
		// System.out.println(data.length);
		// }

		// StringBuilder sb = new StringBuilder();
		// sb.append("A packet captured at ")
		// .append(ph.getTimestamp())
		// .append(":");
		// System.out.println(sb);
		// System.out.println(packet);
	}

	// public static void main( String[] args ){
	//
	//
	// InetAddress addr;
	// try {
	// addr = InetAddress.getByName("192.168.0.103");
	//// addr = InetAddress.getLocalHost();
	// PcapNetworkInterface nif = Pcaps.getDevByAddress(addr);
	//// List<PcapNetworkInterface> networks = Pcaps.findAllDevs();
	////
	//// for (PcapNetworkInterface n : networks){
	//// System.out.println(n.getName()+" : "+n.getAddresses());
	//// }
	//
	//
	// int snapLen = 65536;
	// PromiscuousMode mode = PromiscuousMode.PROMISCUOUS;
	// int timeout = 10000;
	// PcapHandle handle = nif.openLive(snapLen, mode, timeout);
	//
	// Packet packet = handle.getNextPacketEx();
	// handle.close();
	//
	// IpV4Packet ipV4Packet = packet.get(IpV4Packet.class);
	// Inet4Address srcAddr = ipV4Packet.getHeader().getSrcAddr();
	// TcpPacket tcp = ipV4Packet.getPayload().get(TcpPacket.class);
	// System.out.println(srcAddr);
	//
	// } catch (UnknownHostException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (PcapNativeException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (EOFException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (TimeoutException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (NotOpenException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	//
	// System.out.println( "Hello World!" );
	// }

}
