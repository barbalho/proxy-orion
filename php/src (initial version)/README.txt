#Autor: Helber Wagner da Silva
#Email: helber.silva@ifrn.edu.br
#Data: 26/08/2016
#Contexto: Conteúdo de email enviado para orientandos


Orientações para o Código para Controlador de Sessão 


Em anexo, encaminho versão mais recente
do código do proxy que pretende ser (após as melhorias
de vocês) um controlador ao Nível de Sessão
para as subscrições ao Orion Context Broker.
O código "consumidorOrion.html" recebe
dados de uma subscrição do usuário (simulando uma
aplicação interessada em dados acessíveis pelo Orion),
e o código "crossDomainOrionCB.php"
armazena dados da subscrição enviada pela aplicação (atuando como
um proxy) em um BD MySql, repassando em seguida a
mensagem da subscrição ao Orion.

Para funcionar, seguem orientações:

0. Inicializar o serviço do Orion Context Broker no Linux

1. Guardar os arquivos anexos no diretório /var/www/html

2. No Terminal, inicializar o Orion Context Broker

3. No web browser, acessar a URL: localhost/consumidorOrion.html

4. Digitar dados básicos da subscrição a ser enviada ao Orion,
e clicar no botão de submissão

OBS: Recomenda-se instalar um plugin no web browser (e.g., Firebug)
de inspeção de mensagens para visualizar, em um Console, a resposta
que o Orion enviará referente à subscrição

5. Em um Terminal, verificar se a subscrição foi realmente cadastrada no Orion,
executando o comando:

$ curl localhost:1026/v2/subscriptions