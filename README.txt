# Projeto de Controlador de Sessão #

## Descrição ##
Esse projeto tem como objetivo desenvolver um controlador de Sessão para intermediar e otimizar o processo de subscrições ao Orion Context Broker.

## Diretórios ## 
	- docker-files: 
		* Um arquivo de "instalação" do orion através do Docker (https://hub.docker.com/r/fiware/orion/): docker-compose.yml
		* Um script de inicialização do orion pelo Docker: docker_init.sh
	- php/src (initial version):
		* Código fonte php do Controlador de Sessão em sua primeira versão 
	- php/src:
		* Código fonte php do Controlador de Sessão em desenvolvimento
	- terminal-tests:
		* Guia inicial do Orion feitos no terminal (https://fiware-orion.readthedocs.io/en/develop/quick_start_guide/index.html) 
