package net.floodlightcontroller.mactracker;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.projectfloodlight.openflow.protocol.OFBucket;
import org.projectfloodlight.openflow.protocol.OFFactory;
import org.projectfloodlight.openflow.protocol.OFFlowAdd;
import org.projectfloodlight.openflow.protocol.OFGroupAdd;
import org.projectfloodlight.openflow.protocol.OFGroupType;
import org.projectfloodlight.openflow.protocol.OFMessage;
import org.projectfloodlight.openflow.protocol.OFType;
import org.projectfloodlight.openflow.protocol.action.OFAction;
import org.projectfloodlight.openflow.protocol.action.OFActionSetField;
import org.projectfloodlight.openflow.protocol.action.OFActions;
import org.projectfloodlight.openflow.protocol.match.MatchField;
import org.projectfloodlight.openflow.protocol.oxm.OFOxms;
import org.projectfloodlight.openflow.types.DatapathId;
import org.projectfloodlight.openflow.types.EthType;
import org.projectfloodlight.openflow.types.IPv4Address;
import org.projectfloodlight.openflow.types.IpProtocol;
import org.projectfloodlight.openflow.types.MacAddress;
import org.projectfloodlight.openflow.types.OFGroup;
import org.projectfloodlight.openflow.types.OFPort;
import org.projectfloodlight.openflow.types.TransportPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.floodlightcontroller.core.FloodlightContext;
import net.floodlightcontroller.core.IFloodlightProviderService;
import net.floodlightcontroller.core.IOFMessageListener;
import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.core.internal.IOFSwitchService;
import net.floodlightcontroller.core.module.FloodlightModuleContext;
import net.floodlightcontroller.core.module.FloodlightModuleException;
import net.floodlightcontroller.core.module.IFloodlightModule;
import net.floodlightcontroller.core.module.IFloodlightService;
import net.floodlightcontroller.packet.Ethernet;
import net.floodlightcontroller.packet.IPv4;
import net.floodlightcontroller.packet.TCP;
import net.floodlightcontroller.util.FlowModUtils;



public class MACTracker implements IOFMessageListener, IFloodlightModule {

	protected IOFSwitchService switchService;
	protected IFloodlightProviderService floodlightProvider;
	protected Set<Long> macAddresses;
	protected static Logger logger;
	
	private String subscriptionAttribute = null; //Lido do payload
	private String idSensor = null; //Lido do payload

	static List<InfoTable> sessions = new ArrayList<InfoTable>();
	
	@Override
	public String getName() {
		return MACTracker.class.getSimpleName();
	}

	@Override
	public boolean isCallbackOrderingPrereq(OFType type, String name) {
		// TODO Auto-generated method stub
//		if(type.equals(OFType.PACKET_IN) && name.equals("forwarding")){
//			return true;
//		}else{
			return false;
//		}
		
		
	}

	@Override
	public boolean isCallbackOrderingPostreq(OFType type, String name) {
//		if(type.equals(OFType.PACKET_IN) && name.equals("forwarding")){
			return true;
//		}else{
//			return false;
//		}
	}

	@Override
	public Collection<Class<? extends IFloodlightService>> getModuleServices() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<Class<? extends IFloodlightService>, IFloodlightService> getServiceImpls() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<Class<? extends IFloodlightService>> getModuleDependencies() {
		Collection<Class<? extends IFloodlightService>> l = new ArrayList<Class<? extends IFloodlightService>>();
		l.add(IFloodlightProviderService.class);
		return l;
	}

	@Override
	public void init(FloodlightModuleContext context) throws FloodlightModuleException {
		switchService = context.getServiceImpl(IOFSwitchService.class);
		floodlightProvider = context.getServiceImpl(IFloodlightProviderService.class);
		macAddresses = new ConcurrentSkipListSet<Long>();
		logger = LoggerFactory.getLogger(MACTracker.class);
	}

	@Override
	public void startUp(FloodlightModuleContext context) throws FloodlightModuleException {
		
		floodlightProvider.addOFMessageListener(OFType.PACKET_IN, this);
	}

	Map<String,String> map = new HashMap<String,String>();
	
	@Override
	public net.floodlightcontroller.core.IListener.Command receive(IOFSwitch sw, OFMessage msg, FloodlightContext cntx) {
		Command command = Command.CONTINUE;

		switch (msg.getType()) {
		
		case PACKET_IN:
			
			/* Retrieve the deserialized packet in message */
			Ethernet eth = IFloodlightProviderService.bcStore.get(cntx, IFloodlightProviderService.CONTEXT_PI_PAYLOAD);

			if (eth.getEtherType() == EthType.IPv4) {
				
				
				IPv4 ipv4 = (IPv4) eth.getPayload();
				IPv4Address srcIp = ipv4.getSourceAddress();
				IPv4Address dstIp = ipv4.getDestinationAddress();
				
				if (ipv4.getProtocol() == IpProtocol.TCP) {
					
					/*10.0.0.1 is Orion*/
					System.out.println(srcIp.toString()+"-"+dstIp.toString()+" - "+sw.getId());
					if(!srcIp.toString().equals("10.0.0.1") && !dstIp.toString().equals("10.0.0.1")){
						return Command.CONTINUE;
					}
					
					TCP tcp = (TCP) ipv4.getPayload();
					TransportPort srcPort = tcp.getSourcePort();
					TransportPort dstPort = tcp.getDestinationPort();
					
//					if(srcIp.toString().equals("10.0.0.3") && dstIp.toString().equals("10.0.0.1")){
//						System.out.println("Block");
//						return Command.STOP;
//					}
					
					if(srcPort.getPort() != 1026 && dstPort.getPort() != 1026){
						return Command.CONTINUE;
					}
					
					
					command = Command.STOP;

					/*Faz parte de comunica��o com Orion*/
					
					byte payload[] = tcp.getPayload().serialize();
					String payloadText = "";
					boolean initdataJSON = false;
					String dataJSON = "";
					char charRead;
					for(int i = 0; i < payload.length; i++){
						charRead = (char) payload[i];
						payloadText += charRead;
						if(charRead == '{' ){
							initdataJSON = true;
						}
						if(initdataJSON){
							if(charRead != ' ')
								dataJSON += charRead;
						}
						
					}
					
					String key = srcIp.toString() + dstIp.toString();
					if(tcp.getAcknowledge() == 0){
						//if(!map.containsKey(key)){
							map.put(key, dstIp.toString()+srcIp.toString());
							System.out.println("->>1");
							return Command.CONTINUE;
						//}
					}else if(!map.isEmpty() && map.get(dstIp.toString()+srcIp.toString()) != null){
						System.out.println("Out");
						if(payloadText.equals("")){
							
							if(map.get(dstIp.toString()+srcIp.toString()) != null && 
									map.get(dstIp.toString()+srcIp.toString()).equals(key)){
								
								if(map.size() == 1){
									map.put(key, dstIp.toString()+srcIp.toString());
									System.out.println("->>2");
									return Command.CONTINUE;
								}else if(map.size() == 2){
									map.put("TCP_INIT", key);
									System.out.println("->>3");
									return Command.CONTINUE;
								}else if(map.size() == 4 && !map.containsKey("CREATED")){
//									System.out.println("->>5");
									map.put("CREATED", key);
									return Command.CONTINUE;
								}else if(map.containsKey("SUB_ID") && !map.containsKey("FIN1")){
									map.put("FIN1", dstIp.toString()+srcIp.toString());
//									System.out.println("->>1");
									return Command.CONTINUE;
								}
							}
							if(map.containsKey("FIN1") && !map.containsKey("FIN") && map.get("FIN1").equals(key)){
								map.put("FIN", "FIN");
//								return Command.CONTINUE;
							}
						}else{
							if(map.containsKey("TCP_INIT") && map.size() == 3){
								//socket();
								System.out.println("->>4");
								map.put("DATA", dataJSON);
								System.out.println(dataJSON);
								readPayloadData();
								InfoTable infoTable = new InfoTable(
										srcIp.toString(), 
										dstIp.toString(), 
										sw.getId().toString(), 
										eth.getSourceMACAddress().toString(), 
										eth.getDestinationMACAddress().toString(),
										String.valueOf(srcPort.getPort()), 
										String.valueOf(dstPort.getPort()), 
										map.get("DATA"), 
										subscriptionAttribute, 
										idSensor);
								if(!sessions.isEmpty()){
									for(InfoTable it : sessions){
										if(it.getSubscriptionAttribute().equals(infoTable.getSubscriptionAttribute())
												&& it.getIdSensor().equals(infoTable.getIdSensor())
												&& it.getIp_destino().equals(infoTable.getIp_destino())
												&& it.getPort_tcp_destino().equals(infoTable.getPort_tcp_destino())){
											
											it.addSession(infoTable);
											System.out.println(sessions.size());
											System.out.println("------------");
											for(InfoTable inf : sessions){
												System.out.println(inf.toString());
											}
											System.out.println("------------");
											map.clear();
											//System.out.println("->>>> "+sw.getId());
											IOFSwitch iofs = switchService.getSwitch(DatapathId.of("00:00:00:00:00:02"));
											group(iofs);
											////
											return Command.STOP;
										}
									}
								}
								command = Command.CONTINUE;
							}
							if(map.get(dstIp.toString()+srcIp.toString()).equals(key)){
								if(map.containsKey("CREATED") && !map.containsKey("OK")){
									map.put("OK", key);
									return Command.CONTINUE;
								}else if(map.containsKey("OK") && !map.containsKey("SUB_ID")){
									map.put("SUB_ID", "PEGAR_SUB_ID");
									return Command.CONTINUE;
								}
							}
						}
					}

					if(map.containsKey("FIN")){
						readPayloadData();
						InfoTable infoTable = new InfoTable(
													dstIp.toString(), 
													srcIp.toString(), 
													sw.getId().toString(), 
													eth.getDestinationMACAddress().toString(), 
													eth.getSourceMACAddress().toString(),
													String.valueOf(dstPort.getPort()), 
													String.valueOf(srcPort.getPort()), 
													map.get("DATA"), 
													subscriptionAttribute, 
													idSensor);
						
						
						if(sessions.isEmpty()){
							sessions.add(infoTable);
						}else{
							boolean sessionActive = false;
							for(InfoTable it : sessions){
								if(it.getSubscriptionAttribute().equals(infoTable.getSubscriptionAttribute())
										&& it.getIdSensor().equals(infoTable.getIdSensor())
										&& it.getIp_destino().equals(infoTable.getIp_destino())
										&& it.getPort_tcp_destino().equals(infoTable.getPort_tcp_destino())){
									it.addSession(infoTable);
									sessionActive = true;
									break;
								}
							}
							if(!sessionActive){
								sessions.add(infoTable);
							}
						}
						System.out.println(sessions.size());
						System.out.println("------------");
						for(InfoTable inf : sessions){
							System.out.println(inf.toString());
						}
						System.out.println("------------");
						map.clear();
//						group(sw);
						return Command.CONTINUE;
					}
				}else{
					return Command.CONTINUE;
				}
			}else{
				return Command.CONTINUE;
			}
			break;
		default:
			command = Command.CONTINUE;
			break;
		}
		System.out.println(command.name());
		return command;
	}

	private void readPayloadData() {
		JSONParser parser = new JSONParser();
		Object obj;
		try {
			obj = parser.parse(map.get("DATA"));
			JSONObject jsonObject = (JSONObject) obj;
			JSONArray jasonArray = (JSONArray) jsonObject.get("attributes");
			String attribute = (String) jasonArray.get(0);
			
			jasonArray = (JSONArray) jsonObject.get("entities");
			String idEntitie = (String) ((JSONObject) jasonArray.get(0)).get("id");
			
			subscriptionAttribute = attribute;
			idSensor = idEntitie.toString();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	
	
	public void group(IOFSwitch mySwitch){
		//F�brica pra criar os objetos
		OFFactory myFactory = mySwitch.getOFFactory();
		
		//A��es de modifica��o de pacotes		

		ArrayList<OFAction> actionList = createListActions2(mySwitch, "s2-eth2", "10.0.0.4", "00:00:00:00:00:04", 1028);
//		ArrayList<OFAction> actionList = createListActions(mySwitch, "s2-eth3", "10.0.0.3", "00:00:00:00:00:03", 1028);
		ArrayList<OFBucket> buckets = createBuckets(mySwitch, actionList);
		
		/* Crio o grupo com a lista de buckets (cada bucket com uma lista de a��es)*/
		OFGroupAdd groupAdd = myFactory.buildGroupAdd()
			    .setGroup(OFGroup.of(1))
			    .setGroupType(OFGroupType.ALL)
			    .setBuckets(buckets)
			    .build();
		
		/*Escrevo no switch*/
		mySwitch.write(groupAdd);
		
		System.out.println("--------------------GroupMod--------------------");
		
		
		
		/*Crio um fluxo que ir� direcionar os pacotes que obedecem as restri��es para o grupo criado antes*/
		
		OFFlowAdd flowAdd = myFactory.buildFlowAdd()
			    .setHardTimeout(0)
			    .setIdleTimeout(0)
			    .setPriority(FlowModUtils.PRIORITY_MAX)
			    .setMatch(myFactory.buildMatch()
			    	.setExact(MatchField.IN_PORT, mySwitch.getPort("s2-eth1").getPortNo())
//			    	.setExact(MatchField.IN_PORT, mySwitch.getPort("s2-eth3").getPortNo())
			        .setExact(MatchField.ETH_TYPE, EthType.IPv4)
			        .setExact(MatchField.IPV4_SRC, IPv4Address.of("10.0.0.1"))
//			        .setExact(MatchField.IPV4_DST, IPv4Address.of("10.0.0.2"))
			        .setExact(MatchField.IPV4_DST, IPv4Address.of("10.0.0.3"))
			        .setExact(MatchField.IP_PROTO, IpProtocol.TCP)
			       // .setExact(MatchField.TCP_DST, TransportPort.of(1040))
			        .build())
			    .setActions(Collections.singletonList((OFAction) myFactory.actions().buildGroup()
			        .setGroup(OFGroup.of(1))
			        .build()))
			    .build();
	
		
		mySwitch.write(flowAdd);
		
		System.out.println("--------------------FlowMod--------------------");
	}

	private ArrayList<OFBucket> createBuckets(IOFSwitch mySwitch, ArrayList<OFAction> actionList) {
		OFFactory myFactory = mySwitch.getOFFactory();
				
		ArrayList<OFBucket> buckets = new ArrayList<OFBucket>(2);
		
		/* Adicionei uma a��o sem sentido nesse bucket*/
		buckets.add(mySwitch.getOFFactory().buildBucket()
			.setWatchPort(OFPort.ANY)
		    .setWatchGroup(OFGroup.ANY)
		    .setActions(Collections.singletonList((OFAction) myFactory.actions().buildOutput()
		        .setMaxLen(0xffFFffFF)
//		        .setPort(mySwitch.getPort("s2-eth2").getPortNo())
		        .setPort(mySwitch.getPort("s2-eth3").getPortNo())
		        .build()))
		    .build());
		/* Adiciona as a��es no segundo bucket*/
		buckets.add(myFactory.buildBucket()
			.setWatchPort(OFPort.ANY)
		    .setWatchGroup(OFGroup.ANY)
		    .setActions(actionList)
		    .build());
		
		return buckets;
	}


//	private ArrayList<OFAction> createListActions(IOFSwitch mySwitch, String switchPortOutName, String ipHost, String macHost, int port) {
//		//F�brica pra criar os objetos
//		OFFactory myFactory = mySwitch.getOFFactory();
//		
//		//A��es de modifica��o de pacotes
//		ArrayList<OFAction> actionList = new ArrayList<OFAction>();
//		
//		OFOxms oxms = myFactory.oxms();
//		OFActions actions = myFactory.actions();
//		
//		/* Cria a a��o de modificar o destino, e adiciona a lista*/
//		OFActionSetField setIpv4Dst = actions.buildSetField()
//		    .setField(
//		        oxms.buildIpv4Dst()
//		        .setValue(IPv4Address.of(ipHost))
//		        .build()
//		    )
//		    .build();
//		actionList.add(setIpv4Dst);
//		
//		/* Cria a a��o de modificar o MAc destino, e adiciona a lista*/
//		OFActionSetField setEthDst = actions.buildSetField()
//		    .setField(
//		        oxms.buildEthDst()
//		        .setValue(MacAddress.of(macHost))
//		        .build()
//		    )
//		    .build();
//		actionList.add(setEthDst);
//		
//		/* Cria a a��o de modificar o MAc destino, e adiciona a lista*/
//		OFActionSetField setTcpPortDst = actions.buildSetField()
//		    .setField(
//		        oxms.buildTcpDst()
//		        .setValue(TransportPort.of(port)) 
//		        .build()
//		    )
//		    .build();
//		actionList.add(setTcpPortDst);
//		
//		/* Cria a a��o de porta de sa�da do pacote, e adiciona a lista*/
//		actionList.add(myFactory.actions().buildOutput()
//		        .setMaxLen(0xffFFffFF)
//		        .setPort(mySwitch.getPort(switchPortOutName).getPortNo())
//		        .build());
//		
//		return actionList;
//	}
	
	private ArrayList<OFAction> createListActions2(IOFSwitch mySwitch, String switchPortOutName, String ipHost, String macHost, int port) {
		//F�brica pra criar os objetos
		OFFactory myFactory = mySwitch.getOFFactory();
		
		//A��es de modifica��o de pacotes
		ArrayList<OFAction> actionList = new ArrayList<OFAction>();
		
		OFOxms oxms = myFactory.oxms();
		OFActions actions = myFactory.actions();
		
		/* Cria a a��o de modificar o destino, e adiciona a lista*/
		OFActionSetField setIpv4Dst = actions.buildSetField()
		    .setField(
		        oxms.buildIpv4Dst()
		        .setValue(IPv4Address.of("10.0.0.2"))
		        .build()
		    )
		    .build();
		actionList.add(setIpv4Dst);
		
		/* Cria a a��o de modificar o MAc destino, e adiciona a lista*/
		OFActionSetField setEthDst = actions.buildSetField()
		    .setField(
		        oxms.buildEthDst()
		        .setValue(MacAddress.of("00:00:00:00:00:02"))
		        .build()
		    )
		    .build();
		actionList.add(setEthDst);
		
		/* Cria a a��o de modificar o MAc destino, e adiciona a lista*/
		OFActionSetField setTcpPortDst = actions.buildSetField()
		    .setField(
		        oxms.buildTcpDst()
		        .setValue(TransportPort.of(5200)) 
		        .build()
		    )
		    .build();
		actionList.add(setTcpPortDst);
		
		/* Cria a a��o de porta de sa�da do pacote, e adiciona a lista*/
		actionList.add(myFactory.actions().buildOutput()
		        .setMaxLen(0xffFFffFF)
		        .setPort(mySwitch.getPort("s2-eth2").getPortNo())
		        .build());
		
		
		//----Teste
		
		
		/* Cria a a��o de modificar o destino, e adiciona a lista*/
		OFActionSetField setIpSrc = actions.buildSetField()
		    .setField(
		        oxms.buildIpv4Src()
		        .setValue(IPv4Address.of(ipHost))
		        .build()
		    )
		    .build();
		actionList.add(setIpSrc);
		
		/* Cria a a��o de modificar o MAc destino, e adiciona a lista*/
		OFActionSetField setEthSrc = actions.buildSetField()
		    .setField(
		        oxms.buildEthSrc()
		        .setValue(MacAddress.of(macHost))
		        .build()
		    )
		    .build();
		actionList.add(setEthSrc);
		
		/* Cria a a��o de modificar o MAc destino, e adiciona a lista*/
		OFActionSetField setTcpPortSrc = actions.buildSetField()
		    .setField(
		        oxms.buildTcpSrc()
		        .setValue(TransportPort.of(port))
		        .build()
		    )
		    .build();
		actionList.add(setTcpPortSrc);
		
		return actionList;
	}
	
//	public void socket(){
//		
//		try {
//			String USER_AGENT = "Mozilla/5.0";
//			String url = "http://127.0.0.1:1028/accumulate";
//			
//			URL obj = new URL(url);
//			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
//
//			// optional default is GET
//			con.setRequestMethod("GET");
//
//			//add request header
//			con.setRequestProperty("User-Agent", USER_AGENT);
//
//			int responseCode = con.getResponseCode();
//			System.out.println("\nSending 'GET' request to URL : " + url);
//			System.out.println("Response Code : " + responseCode);
//
//			BufferedReader in = new BufferedReader(
//			        new InputStreamReader(con.getInputStream()));
//			String inputLine;
//			StringBuffer response = new StringBuffer();
//
//			while ((inputLine = in.readLine()) != null) {
//				response.append(inputLine);
//			}
//			in.close();
//
//			//print result
//			System.out.println(response.toString());
//			
//		} catch (MalformedURLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//	}
	
	

}
