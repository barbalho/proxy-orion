#!/bin/bash

#Inicializo O Orion
sudo /etc/init.d/contextBroker start

#Inicializo o Accumulate
cd /home/felipe/Downloads/fiware-orion/scripts && ./accumulator-server.py --port 1028 --url /accumulate --host localhost --pretty-print -v

#Abre os arquivos php do proxy no sublime, para edição
sudo sublime3 /var/www/html/consumidorOrion.php /var/www/html/crossDomainOrionCB.php

#abre a página no navegador para executar os testes 
firefox localhost/consumidorOrion.php
