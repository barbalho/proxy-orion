curl 10.0.0.1:1026/v2/entities/Room1/attrs -s -S --header 'Content-Type: application/json' \
     -X PATCH -d @- <<EOF
{
  "temperature": {
    "value": 98.32,
    "type": "Float"
  },
  "pressure": {
    "value": 23,
    "type": "Float"
  }
}
EOF
